//
//  ViewController.swift
//  BMICalculator
//
//  Created by iosdev on 5.4.2020.
//  Copyright © 2020 Ram Mishra. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    private let myPerson = (UIApplication.shared.delegate as! AppDelegate).personObject
    
    var height = Array(100...200).map{
        (Double($0))
    }
    var weight =  Array(30...120).map{
        (Double($0))
    }
    
    
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var editText: UITextField!
    
    @IBOutlet weak var weightText: UITextView!
    @IBOutlet weak var heightText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        editText.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let characters = ["0", "1", "2", "3","4","5", "6", "7", "8","9"]
        for character in characters{
            if string == character{
                print("This characters are not allowed")
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return weight.count
            
        }else{
            return height.count
            
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return String(weight[row])
        }else {
            return String(height[row])
        }
        
    }
    @IBAction func calcBtn(_ sender: UIButton) {
        let userName = String(editText.text ?? "Anonymous user")
        
        let weightValue = weight[pickerView.selectedRow(inComponent: 0)]
        
        
        let heightValue = height[pickerView.selectedRow(inComponent: 1)]
        
        let BMI = myPerson.BMICalc(heightValue, weightValue)
        
        
        if (editText.text == ""){
            
            let alertController = UIAlertController(title: "Alert", message: "Name Field should be filled", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            myLabel.text = ("\(userName)'s BMI is: \(BMI)")
        }
        if ( BMI < 18.5 || BMI > 25){
            myLabel.backgroundColor = UIColor.red
        }else {
            myLabel.backgroundColor = UIColor.green
            
        }
        
    }
}


