//
//  BMICalc.swift
//  BMICalculator
//
//  Created by iosdev on 4.4.2020.
//  Copyright © 2020 Ram Mishra. All rights reserved.
//

import Foundation

class Person {
    
    var logData = [String]()
    
    
    func BMICalc (_ height: Double,_ weight: Double) -> Double{
        let BMI = (weight * 10000)/(height * height)
        logData.append(" BMI of \(weight)KG & \(height)CM is \(String(BMI))")
        return BMI
    }
    
    
    
}
